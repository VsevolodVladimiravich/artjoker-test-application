package com.artjoker.test.testapp.entity

data class OnBoard constructor(
    val title: String,
    val image: String
)