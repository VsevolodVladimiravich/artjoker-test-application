package com.artjoker.test.testapp.feature.onboarding

import com.artjoker.test.testapp.R
import com.artjoker.test.testapp.core.base.BaseActivity
import com.artjoker.test.testapp.databinding.ActivityOnBoardingBinding

class OnBoardingActivity : BaseActivity<ActivityOnBoardingBinding, OnBoardingViewModel>() {

    override fun onBackPressed() {
        viewModel.onBackPressed()
    }

    override fun obtainLayoutId(): Int = R.layout.activity_on_boarding

    override fun obtainViewModelClass() = OnBoardingViewModel::class.java
}