package com.artjoker.test.testapp.feature.main

import com.artjoker.test.testapp.R
import com.artjoker.test.testapp.core.base.BaseActivity
import com.artjoker.test.testapp.databinding.ActivityMainBinding

class MainActivity : BaseActivity<ActivityMainBinding, MainViewModel>() {
    override fun obtainViewModelClass() = MainViewModel::class.java

    override fun obtainLayoutId(): Int = R.layout.activity_main
}
