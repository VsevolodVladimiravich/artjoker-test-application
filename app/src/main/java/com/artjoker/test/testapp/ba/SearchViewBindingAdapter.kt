package com.artjoker.test.testapp.ba

import android.databinding.BindingAdapter
import android.support.v7.widget.SearchView


@BindingAdapter(value = ["onQueryTextSubmit", "onQueryTextChange"], requireAll = false)
fun SearchView.doSetOnQueryTextListener(
    onQueryTextSubmit: OnQueryTextSubmit?,
    onQueryTextChange: OnQueryTextChange?
) {
    when {
        onQueryTextSubmit == null && onQueryTextChange == null -> null

        else -> object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?) =
                onQueryTextSubmit?.run { onQueryTextSubmit(query.orEmpty());true } ?: false

            override fun onQueryTextChange(newText: String?) =
                onQueryTextChange?.run { onQueryTextChange(newText.orEmpty());true } ?: false
        }
    }?.let(::setOnQueryTextListener)
}


interface OnQueryTextSubmit {
    fun onQueryTextSubmit(query: String)
}


interface OnQueryTextChange {
    fun onQueryTextChange(query: String)
}