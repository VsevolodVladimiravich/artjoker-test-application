package com.artjoker.test.testapp.feature.splash

import com.artjoker.test.testapp.core.Screens
import com.artjoker.test.testapp.core.base.BaseViewModel
import com.artjoker.test.testapp.model.prefmanager.PrefHelper
import ru.terrakok.cicerone.Router
import javax.inject.Inject

class SplashViewModel @Inject constructor(
    private val prefHelper: PrefHelper,
    private val router: Router
) : BaseViewModel() {

    @Inject
    fun init() {
        when (prefHelper.wasOnBoardingShown()) {
            true -> Screens.main
            else -> Screens.onBoarding
        }.let(router::replaceScreen)
    }
}