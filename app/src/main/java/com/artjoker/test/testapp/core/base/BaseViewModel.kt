package com.artjoker.test.testapp.core.base

import android.arch.lifecycle.ViewModel
import com.artjoker.test.testapp.util.logE
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

abstract class BaseViewModel : ViewModel() {
    private val disposables: CompositeDisposable = CompositeDisposable()

    override fun onCleared() {
        dispose()
        super.onCleared()
    }

    protected fun dispose() = disposables.dispose()

    protected fun onError(throwable: Throwable) = logE(throwable)

    protected fun <T> Observable<T>.subscribe(success: (t: T) -> Unit) =
        observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                { success(it) },
                { onError(it) },
                { logE("onComplete") }
            ).let(disposables::add)

    protected fun <T> Single<T>.subscribe(success: (t: T) -> Unit) =
        observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                { success(it) },
                { onError(it) }
            ).let(disposables::add)
}