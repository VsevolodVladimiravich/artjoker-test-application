package com.artjoker.test.testapp.core

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import com.artjoker.test.testapp.feature.main.MainActivity
import com.artjoker.test.testapp.feature.onboarding.OnBoardingActivity
import org.jetbrains.anko.intentFor
import ru.terrakok.cicerone.android.support.SupportAppScreen

object Screens {
    val main get() = activityScreen<MainActivity>()
    val onBoarding get() = activityScreen<OnBoardingActivity>()

    private inline fun <reified A : AppCompatActivity> activityScreen() = object : SupportAppScreen() {
        override fun getActivityIntent(context: Context): Intent = context.intentFor<A>()
        override fun getScreenKey(): String = A::class.java.simpleName
    }
}