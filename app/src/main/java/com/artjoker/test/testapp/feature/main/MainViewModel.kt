package com.artjoker.test.testapp.feature.main

import com.artjoker.test.testapp.core.base.BaseViewModel
import com.artjoker.test.testapp.entity.Goods
import com.artjoker.test.testapp.model.repo.GoodsRepository
import io.reactivex.subjects.PublishSubject
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class MainViewModel @Inject constructor(
    private val goodsRepository: GoodsRepository
) : BaseViewModel() {

    private val searchSubject: PublishSubject<String> = PublishSubject.create()
    val adapter: GoodsAdapter = GoodsAdapter()

    @Inject
    fun init() {
        loadAllGoods()
        searchSubject.debounce(300, TimeUnit.MILLISECONDS)
            .filter { !it.isEmpty() }
            .distinctUntilChanged()
            .map { text -> text.toLowerCase().trim() }
            .switchMap { goodsRepository.search(it) }
            .subscribe<List<Goods>> { adapter.replace(it) }
    }

    fun onSearchTextChanged(text: String) {
        searchSubject.onNext(text)
    }

    private fun loadAllGoods() {
        goodsRepository.getAllGoods().subscribe<List<Goods>> { adapter.replace(it) }
    }
}