package com.artjoker.test.testapp.feature.onboarding

import android.databinding.DataBindingUtil
import android.support.constraint.ConstraintLayout
import android.support.v4.view.PagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.artjoker.test.testapp.R
import com.artjoker.test.testapp.databinding.ItemOnBoardBinding
import com.artjoker.test.testapp.entity.OnBoard

class GoodsPagerAdapter constructor(private val items: List<OnBoard>) : PagerAdapter() {

    override fun isViewFromObject(view: View, `object`: Any) = view == `object`

    override fun getCount(): Int = items.size

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val binding = bind(container).apply {
            onBoard = items[position]
        }
        container.addView(binding.root)
        return binding.root
    }

    private fun bind(container: ViewGroup): ItemOnBoardBinding = DataBindingUtil.inflate(
        LayoutInflater.from(container.context),
        R.layout.item_on_board, container,
        false
    )

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as ConstraintLayout)
    }
}