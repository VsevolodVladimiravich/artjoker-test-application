package com.artjoker.test.testapp.core.di

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.artjoker.test.testapp.core.base.BaseViewModel
import com.artjoker.test.testapp.feature.main.MainViewModel
import com.artjoker.test.testapp.feature.onboarding.OnBoardingViewModel
import com.artjoker.test.testapp.feature.splash.SplashViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
interface ViewModelModule {
    @Binds
    fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    fun mainViewModel(viewModel: MainViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(OnBoardingViewModel::class)
    fun onBoardingViewModel(viewModel: OnBoardingViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SplashViewModel::class)
    fun SplashViewModel(viewModel: SplashViewModel): ViewModel
}