package com.artjoker.test.testapp.core.base

import android.databinding.ViewDataBinding
import android.support.v7.widget.RecyclerView

abstract class DataBindingViewHolder<B : ViewDataBinding>(val binding: B) : RecyclerView.ViewHolder(binding.root)