package com.artjoker.test.testapp.core.base


import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.support.annotation.LayoutRes
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import java.util.*

abstract class BaseRVAdapter<M, VH : RecyclerView.ViewHolder> : RecyclerView.Adapter<VH>() {
    private val items: MutableList<M> = ArrayList()

    override fun getItemCount(): Int = this.items.size

    fun add(item: M) {
        this.items.add(item)
        notifyItemInserted(items.size - 1)
    }

    fun add(items: Collection<M>) {
        val insertPosition = this.items.size
        this.items.addAll(items)
        notifyItemRangeInserted(insertPosition, items.size)
    }

    fun replace(items: Collection<M>) {
        this.items.clear()
        notifyDataSetChanged()
        add(items)
    }

    fun clear() {
        val size = this.items.size
        this.items.clear()
        notifyItemRangeRemoved(0, size)
    }

    fun remove(position: Int) {
        items.removeAt(position)
        notifyItemRemoved(position)
    }

    fun getItem(position: Int): M = this.items[position]


    fun getItems(): List<M> = items

    protected fun <B : ViewDataBinding> binding(parent: ViewGroup, @LayoutRes layoutRes: Int): B =
        DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            layoutRes, parent, false
        )
}