package com.artjoker.test.testapp.core.di

import dagger.Module
import dagger.Provides
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import javax.inject.Singleton

@Module
class CiceroneModule {
    @Singleton
    @Provides
    fun provideNavigation(): Cicerone<Router> = Cicerone.create()

    @Provides
    fun router(cicerone : Cicerone<Router>): Router = cicerone.router

    @Provides
    fun navigatorHolder(cicerone : Cicerone<Router>): NavigatorHolder = cicerone.navigatorHolder
}