package com.artjoker.test.testapp.core.di

import android.app.Application
import android.content.Context
import com.artjoker.test.testapp.model.prefmanager.PrefHelper
import com.artjoker.test.testapp.model.prefmanager.PreferenceHelper
import com.artjoker.test.testapp.model.repo.GoodsRepository
import com.artjoker.test.testapp.model.repo.GoodsRepositoryImpl
import dagger.Binds
import dagger.Module
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Module(includes = [(AndroidSupportInjectionModule::class)])
interface AppModule {
    @Singleton
    @Binds
    fun provideContext(app: Application): Context

    @Singleton
    @Binds
    fun provideNewsRepo(repo: GoodsRepositoryImpl): GoodsRepository

    @Singleton
    @Binds
    fun providePrefHelper(helper: PreferenceHelper): PrefHelper
}