package com.artjoker.test.testapp.util

import android.util.Log
import com.artjoker.test.testapp.BuildConfig

private const val DEFAULT_TAG = BuildConfig.APP_VERSION_NAME

fun Any.logD(message: String?) {
    if (BuildConfig.DEBUG)
        Log.d(DEFAULT_TAG, message.orEmpty())
}

fun Any.logE(message: String?) {
    if (BuildConfig.DEBUG)
        Log.e(DEFAULT_TAG, message.orEmpty())
}

fun Any.logE(throwable: Throwable) {
    if (BuildConfig.DEBUG) {
        throwable.printStackTrace()
        Log.e(DEFAULT_TAG, throwable.message, throwable)
    }
}