package com.artjoker.test.testapp.feature.onboarding

import android.databinding.ObservableInt
import com.artjoker.test.testapp.core.Screens
import com.artjoker.test.testapp.core.base.BaseViewModel
import com.artjoker.test.testapp.entity.OnBoard
import com.artjoker.test.testapp.model.prefmanager.PrefHelper
import ru.terrakok.cicerone.Router
import javax.inject.Inject

class OnBoardingViewModel @Inject constructor(
    private val prefHelper: PrefHelper,
    private val router: Router
) : BaseViewModel() {

    val viewPagerCurrentPosition: ObservableInt = ObservableInt(0)
    val adapter: GoodsPagerAdapter = GoodsPagerAdapter(getItems())

    fun showMainScreen() {
        prefHelper.setOnBoardingShown(true)
        router.replaceScreen(Screens.main)
    }

    fun onBackPressed() {
        when (viewPagerCurrentPosition.get()) {
            0 -> router.replaceScreen(Screens.main)
            else -> viewPagerCurrentPosition.set(viewPagerCurrentPosition.get() - 1)
        }
    }

    private fun getItems(): List<OnBoard> = mutableListOf<OnBoard>().apply {
        val imageUrl =
            "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e1/Android_dance.svg/721px-Android_dance.svg.png"
        add(OnBoard("first", imageUrl))
        add(OnBoard("second", imageUrl))
        add(OnBoard("third", imageUrl))
    }

    companion object {
        const val LAST_ITEM_POSITION = 2
    }
}
