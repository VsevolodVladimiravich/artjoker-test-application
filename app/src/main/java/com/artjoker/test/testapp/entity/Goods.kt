package com.artjoker.test.testapp.entity

data class Goods constructor(
    val title: String,
    val image: String
)