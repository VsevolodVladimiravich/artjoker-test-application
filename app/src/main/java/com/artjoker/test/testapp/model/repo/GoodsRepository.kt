package com.artjoker.test.testapp.model.repo

import com.artjoker.test.testapp.entity.Goods
import io.reactivex.Observable
import io.reactivex.Single

interface GoodsRepository {
    fun search(query: String): Observable<List<Goods>>

    fun getAllGoods(): Single<List<Goods>>
}