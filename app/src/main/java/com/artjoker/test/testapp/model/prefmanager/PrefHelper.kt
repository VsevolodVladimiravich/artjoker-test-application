package com.artjoker.test.testapp.model.prefmanager

interface PrefHelper {
    fun wasOnBoardingShown(): Boolean

    fun setOnBoardingShown(shown: Boolean)
}