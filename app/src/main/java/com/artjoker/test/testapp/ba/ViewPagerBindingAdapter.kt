package com.artjoker.test.testapp.ba

import android.databinding.BindingAdapter
import android.databinding.BindingConversion
import android.databinding.InverseBindingAdapter
import android.databinding.InverseBindingListener
import android.support.v4.view.ViewPager
import android.view.View

@BindingAdapter("currentItem")
fun ViewPager.setCurrentItemPosition(value: Int) {
    currentItem = value
}

@InverseBindingAdapter(attribute = "currentItem")
fun ViewPager.getCurrentItemPosition(): Int = currentItem

@BindingAdapter("currentItemAttrChanged")
fun ViewPager.setListener(listener: InverseBindingListener?) {
    addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
        override fun onPageScrollStateChanged(state: Int) {
        }

        override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
            listener?.onChange()
        }

        override fun onPageSelected(position: Int) {
        }
    })
}

@BindingConversion
fun visibility(visible: Boolean): Int = if (visible) View.VISIBLE else View.GONE