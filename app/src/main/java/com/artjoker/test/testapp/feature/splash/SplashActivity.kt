package com.artjoker.test.testapp.feature.splash

import com.artjoker.test.testapp.R
import com.artjoker.test.testapp.core.base.BaseActivity
import com.artjoker.test.testapp.databinding.ActivitySplashBinding

class SplashActivity : BaseActivity<ActivitySplashBinding, SplashViewModel>() {
    override fun obtainLayoutId(): Int = R.layout.activity_splash

    override fun obtainViewModelClass(): Class<SplashViewModel> = SplashViewModel::class.java
}