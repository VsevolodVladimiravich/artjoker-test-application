package com.artjoker.test.testapp.feature.main

import android.view.ViewGroup
import com.artjoker.test.testapp.R
import com.artjoker.test.testapp.core.base.BaseRVAdapter
import com.artjoker.test.testapp.core.base.DataBindingViewHolder
import com.artjoker.test.testapp.databinding.ItemGoodsBinding
import com.artjoker.test.testapp.entity.Goods

class GoodsAdapter : BaseRVAdapter<Goods, GoodsAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder = ViewHolder(
        binding(parent, R.layout.item_goods)
    )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.item = getItem(position)
    }

    inner class ViewHolder(binding: ItemGoodsBinding) : DataBindingViewHolder<ItemGoodsBinding>(binding)
}