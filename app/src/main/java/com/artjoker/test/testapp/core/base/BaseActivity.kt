package com.artjoker.test.testapp.core.base

import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.v7.app.AppCompatActivity
import com.artjoker.test.testapp.BR
import com.artjoker.test.testapp.R
import dagger.android.AndroidInjection
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.android.support.SupportAppNavigator
import javax.inject.Inject

abstract class BaseActivity<B : ViewDataBinding, VM : BaseViewModel> : AppCompatActivity() {

    @Inject
    protected lateinit var viewModelFactory: ViewModelProvider.Factory
    @Inject
    protected lateinit var navigationHolder: NavigatorHolder
    protected lateinit var viewModel: VM
    protected lateinit var binding: B
    protected open val navigator: SupportAppNavigator = SupportAppNavigator(this, R.id.fragment_container)

    private val lifecycleLogger = LifecycleLogger(this.javaClass.simpleName)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AndroidInjection.inject(this)
        lifecycle.addObserver(lifecycleLogger)
        bind()
    }

    override fun onResume() {
        super.onResume()
        navigationHolder.setNavigator(navigator)
    }

    override fun onPause() {
        navigationHolder.removeNavigator()
        super.onPause()
    }

    @LayoutRes
    protected abstract fun obtainLayoutId(): Int

    protected abstract fun obtainViewModelClass(): Class<VM>

    private fun bind() {
        binding = DataBindingUtil.setContentView(this, obtainLayoutId())
        viewModel = provideViewModel(obtainViewModelClass())
        binding.setVariable(BR.viewModel, viewModel)
        binding.executePendingBindings()
    }

    private fun <VM : BaseViewModel> provideViewModel(viewModelClass: Class<VM>) =
        ViewModelProviders.of(this, viewModelFactory)[viewModelClass]
}