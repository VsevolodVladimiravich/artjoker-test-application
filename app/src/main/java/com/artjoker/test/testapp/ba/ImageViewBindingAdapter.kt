package com.artjoker.test.testapp.ba

import android.databinding.BindingAdapter
import android.widget.ImageView
import com.bumptech.glide.Glide

@BindingAdapter("imageUrl")
fun setImage(view: ImageView, url: String?) {
    Glide.with(view)
        .load(url)
        .into(view)
}