package com.artjoker.test.testapp.model.prefmanager

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import javax.inject.Inject

class PreferenceHelper @Inject constructor(context: Context) : PrefHelper {

    companion object {
        private const val KEY_WAS_ON_BOARDING_SHOWN = "com.artjoker.test.was_shown"
    }

    private val prefs: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)

    override fun wasOnBoardingShown(): Boolean = prefs.getBoolean(KEY_WAS_ON_BOARDING_SHOWN, false)

    override fun setOnBoardingShown(shown: Boolean) {
        save(KEY_WAS_ON_BOARDING_SHOWN, shown)
    }

    private fun save(key: String, value: Boolean) {
        prefs.edit().putBoolean(key, value).apply()
    }
}