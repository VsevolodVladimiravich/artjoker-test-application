package com.artjoker.test.testapp.core.di

import com.artjoker.test.testapp.feature.main.MainActivity
import com.artjoker.test.testapp.feature.onboarding.OnBoardingActivity
import com.artjoker.test.testapp.feature.splash.SplashActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
interface ActivityBuilder {
    @ContributesAndroidInjector
    fun contributeSplashActivity(): SplashActivity

    @ContributesAndroidInjector
    fun contributeMainActivity(): MainActivity

    @ContributesAndroidInjector
    fun contributeOnBoardingActivity(): OnBoardingActivity
}