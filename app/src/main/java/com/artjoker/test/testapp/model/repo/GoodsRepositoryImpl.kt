package com.artjoker.test.testapp.model.repo

import com.artjoker.test.testapp.entity.Goods
import io.reactivex.Observable
import io.reactivex.Single
import java.util.*
import javax.inject.Inject

class GoodsRepositoryImpl @Inject constructor() : GoodsRepository {
    private val goods: List<Goods> = mockGoods()

    override fun search(query: String): Observable<List<Goods>> = Observable.just(goods)
        .flatMap { Observable.fromIterable(it) }
        .filter { it.title.contains(query) }
        .toList()
        .toObservable()

    override fun getAllGoods(): Single<List<Goods>> = Single.just(goods)

    private fun mockGoods(): List<Goods> {
        val goods: MutableList<Goods> = mutableListOf()
        for (i in 1..100) {
            goods.add(createGoods())
        }
        return goods
    }

    private fun createGoods() = Goods(
        getRandomString(),
        getImage()
    )

    private fun getImage(): String = "https://wallpaperbrowse.com/media/images/750801.png"

    private fun getRandomString() = (1..10)
        .map { (Random().nextInt(('a'..'z').endInclusive.toInt() - ('a'..'z').start.toInt()) + ('a'..'z').start.toInt()).toChar() }
        .joinToString("")
}